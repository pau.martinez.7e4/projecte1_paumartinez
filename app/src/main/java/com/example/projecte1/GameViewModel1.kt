package com.example.projecte1

import androidx.lifecycle.ViewModel

class GameViewModel1 : ViewModel() {
    // Game data model
    var imatges = arrayOf(R.drawable.castor, R.drawable.oso, R.drawable.pez, R.drawable.castor, R.drawable.oso, R.drawable.pez)
    var cartes = mutableListOf<Carta>()

    var contador: Int = 0

    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        imatges.shuffle()
        for(i in 0..5) {
            cartes.add(Carta(i, imatges[i]))
        }
    }

}