package com.example.projecte1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import androidx.lifecycle.ViewModelProvider

class InGameActivity1 : AppCompatActivity(), View.OnClickListener {
    // UI Views
    private lateinit var carta1: ImageButton
    private lateinit var carta2: ImageButton
    private lateinit var carta3: ImageButton
    private lateinit var carta4: ImageButton
    private lateinit var carta5: ImageButton
    private lateinit var carta6: ImageButton
    private lateinit var opcio1: Carta
    private lateinit var opcio2: Carta
    private lateinit var stopButton: Button

    // ViewModel declaration
    private lateinit var viewModel: GameViewModel1

//referencia: --> https://github.com/cavanosa/TwoFaces/blob/master/app/src/main/java/com/proba/cavanosa/probadoscaras/Juego.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_in_game1)
        viewModel = ViewModelProvider(this).get(GameViewModel1::class.java)

        opcio1 = viewModel.cartes[0]
        opcio2 = viewModel.cartes[0]
        carta1 = findViewById(R.id.imageView)
        carta2 = findViewById(R.id.imageView2)
        carta3 = findViewById(R.id.imageView3)
        carta4 = findViewById(R.id.imageView4)
        carta5 = findViewById(R.id.imageView5)
        carta6 = findViewById(R.id.imageView6)
        stopButton = findViewById(R.id.button)

        carta1.setOnClickListener(this)
        carta2.setOnClickListener(this)
        carta3.setOnClickListener(this)
        carta4.setOnClickListener(this)
        carta5.setOnClickListener(this)
        carta6.setOnClickListener(this)
        updateUI()
    }

    override fun onClick(v: View?) {
        gameBoolean()
        when(v) {
            carta1 -> {
                if (opcio1.imatgeId != viewModel.cartes[0].imatgeId){
                    opcio1 = viewModel.cartes[0]
                }else if (opcio2.imatgeId != viewModel.cartes[0].imatgeId) {
                    opcio2 = viewModel.cartes[0]
                    gameBoolean()
                }
                girarCarta(0, carta1)
            }
            carta2 -> {
                if (opcio1.imatgeId != viewModel.cartes[1].imatgeId){
                    opcio1 = viewModel.cartes[1]
                }else if (opcio2.imatgeId != viewModel.cartes[1].imatgeId) {
                    opcio2 = viewModel.cartes[1]
                    gameBoolean()
                }
                girarCarta(1, carta2)
            }
            carta3 -> {
                if (opcio1.imatgeId != viewModel.cartes[2].imatgeId){
                    opcio1 = viewModel.cartes[2]
                }else if (opcio2.imatgeId != viewModel.cartes[2].imatgeId) {
                    opcio2 = viewModel.cartes[2]
                    gameBoolean()
                }
                girarCarta(2, carta3)
            }
            carta4 -> {
                if (opcio1.imatgeId != viewModel.cartes[3].imatgeId){
                    opcio1 = viewModel.cartes[3]
                }else if (opcio2.imatgeId != viewModel.cartes[3].imatgeId) {
                    opcio2 = viewModel.cartes[3]
                    gameBoolean()
                }
                girarCarta(3, carta4)
            }
            carta5 -> {
                if (opcio1.imatgeId != viewModel.cartes[4].imatgeId){
                    opcio1 = viewModel.cartes[4]
                }else if (opcio2.imatgeId != viewModel.cartes[4].imatgeId) {
                    opcio2 = viewModel.cartes[4]
                    gameBoolean()
                }
                girarCarta(4, carta5)
            }
            carta6 -> {
                if (opcio1.imatgeId != viewModel.cartes[5].imatgeId){
                    opcio1 = viewModel.cartes[5]
                }else if (opcio2.imatgeId != viewModel.cartes[5].imatgeId) {
                    opcio2 = viewModel.cartes[5]
                    gameBoolean()
                }
                girarCarta(5, carta6)
            }
        }
    }

    private fun gameBoolean(){
        if (opcio1.imatgeId != opcio2.imatgeId){
            girarCarta(0, carta1, true)
            girarCarta(1, carta2, true)
            girarCarta(2, carta3, true)
            girarCarta(3, carta4, true)
            girarCarta(4, carta5, true)
            girarCarta(5, carta6, true)
        }
    }



    // Funció que utilitzarem per girar la carta
    private fun girarCarta(idCarta: Int, carta: ImageView, giradaInicial: Boolean = false)  {
        viewModel.contador++
        if (giradaInicial) {
            carta.setImageResource(R.drawable.logo2)
            viewModel.cartes[idCarta].girada = false
        } else {
            if (!viewModel.cartes[idCarta].girada) {
                carta.setImageResource(viewModel.cartes[idCarta].imatgeId)
                viewModel.cartes[idCarta].girada = true
            } else {
                carta.setImageResource(R.drawable.logo2)
                viewModel.cartes[idCarta].girada = false
            }
        }
    }

    private fun updateUI() {
        if(viewModel.cartes[0].girada)
            carta1.setImageResource(viewModel.cartes[0].imatgeId)
        if(viewModel.cartes[1].girada)
            carta2.setImageResource(viewModel.cartes[1].imatgeId)
        if(viewModel.cartes[2].girada)
            carta3.setImageResource(viewModel.cartes[2].imatgeId)
        if(viewModel.cartes[3].girada)
            carta4.setImageResource(viewModel.cartes[3].imatgeId)
        if(viewModel.cartes[4].girada)
            carta5.setImageResource(viewModel.cartes[4].imatgeId)
        if(viewModel.cartes[5].girada) {
            carta6.setImageResource(viewModel.cartes[5].imatgeId)
        }
    }
}

