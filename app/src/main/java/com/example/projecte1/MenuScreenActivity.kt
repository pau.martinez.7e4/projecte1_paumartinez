package com.example.projecte1

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner

class MenuScreenActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    lateinit var gameButton: Button
    var counter = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super<AppCompatActivity>.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_screen)


        gameButton = findViewById(R.id.button2)

        gameButton.setOnClickListener {
            val intent = Intent(this, InGameActivity1::class.java)
            if (counter == 1){
                //actualitza el counter per afegir InGameActivity2::class.java
            }else if (counter == 2){
                //actualitza el counter per afegir InGameActivity3::class.java
            }

            startActivity(intent)
        }
        val spinner: Spinner = findViewById(R.id.spinner)
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.spinner_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        if (parent != null) {
            if (parent.getItemAtPosition(0) == parent.getItemAtPosition(position)){
                counter = 0
            }else if (parent.getItemAtPosition(1) == parent.getItemAtPosition(position)){
                counter = 1
            }else if (parent.getItemAtPosition(3) == parent.getItemAtPosition(position)){
                counter = 2
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        counter = 0
    }
}
